#!/bin/bash -

set -o nounset                              # Treat unset variables as an error
set -ex

MAINTAINER_NAME='Exante Ops Team'
MAINTAINER_EMAIL='ops-team@exante.eu'
BUILD_CMD="debuild -b -uc -us"
BUILD_LOG="../build.log"


function _prepare_current_version () {

    TARBALL=$( uscan --download-current-version --rename | grep 'and renamed it as' | awk '{print $NF}' )

    if [ -f ../${TARBALL} ]; then
        tar zxf ../${TARBALL} --strip 1
    else
        echo "Tarball ${TARBALL} failed to download. Build aborted"
        exit 1
    fi

    return 0

}

function _prepare_new_upstream_version () {

    TARBALL=$( uscan --rename | grep 'and renamed it as' | awk '{print $NF}' )

    if [ -f ../${TARBALL} ]; then
        NEW_BUILD_ROOT=$( DEBFULLNAME=${MAINTAINER_NAME} DEBEMAIL=${MAINTAINER_EMAIL} uupdate ../${TARBALL} | grep 'to see the new package' | awk '{gsub(/"/, "", $4); print $4}' )
    else
        echo "New upstream version traball ${TARBALL} failed to download. Build aborted."
        exit 2
    fi

    cd ${NEW_BUILD_ROOT}

    return 0

}

function _build_package () {

    if ! ${BUILD_CMD} >${BUILD_LOG} 2>&1; then
        echo "Build failed. Check build log in the workspace."
        exit 3
    fi

    return 0

}

# cd infrastructure-supervisor-dpkg

if ! uscan --report >/dev/null 2>&1; then
    # Current version is the latest one
    echo Current version is the latest one
    _prepare_current_version
else
    # New upstream version found
    echo New upstream version found
    _prepare_new_upstream_version
fi

_build_package

exit 0
